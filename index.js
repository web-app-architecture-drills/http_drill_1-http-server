const http = require('http');
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');
const url = require('url');

let server = http.createServer( (request, response) => {
    
    try{
        if(request.url === '/GET/html'){
            fs.readFile(path.join(__dirname, 'public', 'index.html'), (err, data) => {
                if(err){
                    response.end("Error reading html file: "+ err.message);
                }
                else{
                    response.end(data);
                }
            })
        }
        if(request.url === '/GET/json'){
            fs.readFile(path.join(__dirname, 'public', 'string.json'), "utf-8", (err, data) => {
                if(err){
                    response.end("Error reading JSON file");
                }
                else{
                    response.writeHead(200, {'Content-Type': 'application/json'});
                    response.end(data);
                }
            })
        }
        if(request.url === '/GET/uuid'){
            let uuidObject = { "uuid": uuid.v4() };
            response.end(JSON.stringify(uuidObject));
        }
        
        const parsedUrl = url.parse(request.url);
        const urlPathName = parsedUrl.pathname;
        const urlPathArray = urlPathName.split("/");
        const status_code = urlPathArray[urlPathArray.length-1];
        const delay_in_seconds = urlPathArray[urlPathArray.length-1];
    
        if(request.url === '/GET/status/'+status_code){
            response.end(http.STATUS_CODES[status_code]);
        }
        if(request.url === '/GET/delay/'+ delay_in_seconds){
            setTimeout( () => {
                response.end(http.STATUS_CODES[200]);
            }, delay_in_seconds*1000);
        }

    }catch(err){
        response.end(err.message);
    }

})

server.listen(8000, ()=> console.log("Static server created"));